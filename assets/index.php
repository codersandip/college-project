<?php 
$conn = mysqli_connect('127.0.0.1','root','','ci');
$query = "SELECT * FROM `countries`" ;
$run = mysqli_query($conn,$query);

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
 </head>
 <body>
 	<div class="container">
 		<table class="table text-center w-50 table-bordered table-striped">
 			<thead>
 				<tr>
 					<th>Sr.No.</th>
 					<th>Short Name</th>
 					<th>Name</th>
 					<th>Phone Code</th>
 				</tr>
 			</thead>
 		<?php
 			while ($data = mysqli_fetch_assoc($run)) {
?>
				<tr>
					<th><?= $data['id']; ?></th>
					<th><?= $data['sortname']; ?></th>
					<th><?= $data['name']; ?></th>
					<th><?= $data['phonecode']; ?></th>
				</tr>
<?php
			}
?>
 		</table>
 		<button class="btn btn-outline-primary w-25" id="print">Print</button>
 	</div>
 	<script type="text/javascript" src="js/bootstrap.js"></script>
 	<script type="text/javascript" src="js/jquery.js"></script>
 	<script type="text/javascript">
 		$('document').ready(function(){
 			$('#print').click(function(){
 				$(this).attr('style','display:none');
 				print();
 				// alert(this);
 			});
 		});
 	</script>
 </body>
</html>