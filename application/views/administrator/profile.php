<?php 
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 </head>
 <body class="background2">
 	<?php include 'header.php'; ?>
 	<div class="container mt-5">
 			<h1 class="text-center">Profile</h1>
 			<?= form_open('Administrator/Profile_Validate'); ?>
 			<table class="table col-md-8 offset-md-2 mt-5 table-striped table-bordered rounded">
 				<tr>
 					<th>First Name :-</th>
 					<td><?= form_input(['name'=>'first_name','class'=>'form-control','value'=>set_value('first_name',$data->first_name)]).form_error('first_name') ?></td>
 				</tr>
 				<tr>
 					<th>Last Name :-</th>
 					<td><?= form_input(['name'=>'last_name','class'=>'form-control','value'=>set_value('first_name',$data->last_name)]).form_error('last_name') ?></td>
 				</tr>
 				<tr>
 					<th>Email No. :-</th>
 					<td><?= form_input(['name'=>'email','class'=>'form-control','value'=>set_value('email',$data->email)]).form_error('email') ?></td>
 				</tr>
 				<tr>
 					<th>Mobile No. :-</th>
 					<td><?= form_input(['name'=>'mobile_no','class'=>'form-control','value'=>set_value('mobile_no',$data->mobile_no)]).form_error('mobile_no') ?></td>
 				</tr>
 				<tr>
 					<th colspan="2">
 						<?= form_submit('','Update Profile',['class'=>'btn btn-outline-info w-50']).
 						form_submit('','Cancel',['class'=>'btn btn-outline-warning ml-5 w-25'])
 						 ?>
 					</th>
 				</tr>

 			</table>
 	</div>
 	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
 </body>
 </html>