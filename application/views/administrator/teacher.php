<!DOCTYPE html>
<html>
<head>
	<title>
		
	</title>
</head>
<body class="background2">
	<?php include 'header.php'; ?>
	<div class="container my-5">
		<div class="display-3 text-center text-info strong font-weight-bold font-italic">Teacher Registration</div>
		<div class="row my-3 animated swing">
			<div class="col-md-6 offset-md-5 border rounded">
				<div class="form-group mt-4	">
					<form action="<?= base_url('Administrator/TeacherV') ?>" method="post">
						<input type="hidden" name="login_type" value="Teacher">
						<label>First Name :-</label>
						<input type="text" name="first_name" class="form-control" placeholder="First Name">
						<?= form_error('first_name') ?><br>
						
						<label>Last Name :-</label>
						<input type="text" name="last_name" class="form-control" placeholder="Last Name">
						<?= form_error('last_name') ?><br>
						
						<label>Email :-</label>
						<input type="text" name="email" class="form-control" placeholder="Email">
						<?= form_error('email') ?><br>
						
						<label>Mobile No. :-</label>
						<input type="text" name="mobile_no" class="form-control" placeholder="Mobile No.">
						<?= form_error('mobile_no') ?><br>
						
						<label>Branch</label>
						<select class="custom-select" name="branch"  value="">
							<option></option>
							<option value="Computer Engg">Computer Engg</option>
							<option value="Mechanical Engg">Mechanical Engg</option>
							<option value="Civil Engg">Civil Engg</option>
							<option value="Electronic And Telecomunication Engg">Electronic And Telecomunication Engg</option>
						</select>
						<?= form_error('branch') ?>
						<br>
						<hr>
						<input type="submit" value="Register" name="" class="btn btn-outline-primary w-25">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="Reset" class="btn btn-danger w-25">
					</form>
				</div>
			</div>
		</div>

	</div>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>