<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background2">
	<?php include 'header.php'; ?>
	<div class="container">
		<div class="col-md-8 offset-md-2 border mt-5 animated slideInUp">
			<h1 class="text-center pt-3">*  Complaint  *</h1>
			<hr>
			<div class="row">
				<div class="col-12 h3">
					<b>Complainter Name :- </b><i><?php echo $data->complainter_name ?></i>
				</div>
			</div>
			<hr class="my-4">
			<div class="row">
				<div class="col-4">
					<h3><b>Subject :-</b></h3>
				</div>
				<div class="col-8">
					<h5><?= $data->complaint_subject ?></h5>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-3 h3">
					<b>Body</b>
				</div>
				<div class="col-9">
					<h6><?= $data->complaint_body ; ?></h6>
				</div>
			</div>


			<div class="form-group">
				<h4 class="text-center"><b>Reply</b></h4>
				<div>
					<?= form_open('Home/ComplaintReply').form_hidden('email',$data1->email).form_hidden('id',$data->id).form_textarea(['name'=>'reply','class'=>'form-control','placeholder'=>'Reply','required'=>'']).br().form_hidden('title',$data->complaint_subject); ?>
					<div class="d-flex justify-content-center">
						<?= form_submit('','Reply',['class'=>'btn btn-primary w-25']).form_close() ?>
					</div>
				</div>
			</div>


		</div>
	</div>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>