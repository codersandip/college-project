<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background4">
	<?php include 'header.php'; ?>
	<div class="container">
		<div class="row my-5">
			<div class="background3 col-xl-6 p-3 col-lg-6 col-md-6 col-sm-8 offset-xl-3 border offset-lg-3 offset-md-3 rounded border-dark offset-sm-2 zoomInDown animated">
				<div class="text-center text-dark font-italic font-weight-bold bg-default" style="font-size: 45">
					 Login
				</div>
				<hr>
				<?= $this->session->flashdata('msg'); ?>
				<div>
					<?= form_open('Administrator/Login')?>
					<select class="form-control" name="login_type">
						<option>Administrator</option>
						<option>Principle</option>
					</select>
					<?= br().'<span class="fa fa-user"></span> '.form_label('Email :').form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Email','value'=>set_value('email')]).form_error('email').br().'<span class="fa fa-lock"></span> '.form_label('Password :').form_password(['name'=>'password','class'=>'form-control','placeholder'=>'Password','value'=>set_value('password')]).form_error('password').br() ?>
					<div class="text-center">
						<?= form_submit('','Login',['class'=>'btn btn-outline-primary w-25']).nbs(10).form_reset('','Reset',['class'=>'btn btn-danger w-25']) ?>
					</div>
					<hr>
					<div>
						Forgot <?= anchor('Home/Forgot','Password?'). nbs(15); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#msg').fadeOut(5000);
		});
	</script>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>