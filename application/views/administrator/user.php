<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background2">
	<?php include 'header.php'; ?>
	<div class="container mt-5">
		<div class="display-4 text-center">
			User List
		</div>
		<div class=" p-1">
			<form action="<?= base_url('Administrator/user_update') ?>" method="post">
			<table class="table table-responsive text-center table-bordered table-striped" id="myTable">
				<thead>
					<th>Sr.No.</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Mobile No.</th>
					<th>Email</th>
					<th>Enrollment No.</th>
					<th>Branch</th>
					<th>Destination</th>
				</thead>
				<tbody id="data">
				<?php			
						 $i=0;
		foreach ($data as $data) {
			# code...
	$i++; ?>
					<tr>
						<input type="hidden" name="id[]" value="<?= $data->id ?>">
					<th><?= $i ?></th>
					<td><input type="text"  class="form-control-sm" name="first_name[]" value="<?= $data->first_name ?>"></td>
					<td><input type="text"  class="form-control-sm" name="last_name[]" value="<?= $data->last_name ?>"></td>
					<td><input type="text"  class="form-control-sm" name="mobile_no[]" value="<?= $data->mobile_no ?>"></td>
					<td><input type="text"  class="form-control-sm" readonly name="email[]" value="<?= $data->email ?>"></td>
					<td><input type="text"  class="form-control-sm" name="enrollment_no[]" value="<?= $data->enrollment_no ?>"></td>
					<td><select class="form-control-sm" name="branch[]" value="<?= set_value('branch',$data->branch)?>">
							<option value="<?= set_value('branch',$data->branch)?>"><?= set_value('Branch',$data->branch)?></option>
							<option value="Computer Engg">Computer Engg</option>
							<option value="Mechanical Engg">Mechanical Engg</option>
							<option value="Civil Engg">Civil Engg</option>
							<option value="Electronic And Telecomunication Engg">Electronic And Telecomunication Engg</option>
						</select></td>
					<td><!-- <input type="text"  value="<?= $data->login_type ?>"> -->
					<select class="form-control-sm" name="login_type[]">
						<option><?= $data->login_type ?></option>
						<option>Student</option>
						<option>Teacher</option>
						<option>Parrent</option>
					</select></td>
			</tr>
		<?php } ?>
				</tbody>
			</table>	
							<input type="submit" value="Update" class="btn btn-outline-primary w-25">
		</div>
	</div>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
			$('#msg').fadeOut(5000);
	});
</script>
</html>