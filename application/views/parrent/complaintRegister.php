<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background3">
	<?php include 'header.php';?>
	<div class="container">
		<div class="display-4 mt-5 animated slideInRight">
			Complaint Register
		</div>
		<?= $this->session->flashdata('msg'); ?>
			<form action="<?php echo base_url(); ?>Parrent/ComplaintRegisterValidate" method="post" class="animated zoomIn mb-5">
				<div class="row mt-4">
					<div class="col-md-6">
						
						<input type="hidden" name="complainter_id" value="<?= $this->session->userdata('id') ?>">
						<input type="hidden" name="complainter_type" value="Student">

						<label class="mt-3">Name of Complainter :</label>
						
						<input type="text" name="complainter_name" readonly placeholder="Name" required="" class="form-control" value="<?= $this->session->userdata('first_name').' '. $this->session->userdata('last_name'); ?>">
						
						<label class="mt-3">Branch :</label>
						
						<input type="text" name="branch" readonly placeholder="Branch" required="" class="form-control" value="<?= $this->session->userdata('branch') ?>">

						<label class="mt-3">Enrollment No. :</label>

						<input type="text" name="enrollment_no" readonly placeholder="Enrollment No." required="" class="form-control" value="<?= $this->session->userdata('enrollment_no') ?>">

					</div>
					<div class="col-md-6">
						<label class="mt-3">Complaint Send To :</label>


						<select class="custom-select form-group" name="reciever_id" required="" id="reciever_id">
							
						</select>
						
						<label class="mt-3">Complaint Subject</label>
						
						<textarea class="form-control" rows="5" name="complaint_subject" required="" placeholder="Complaint Subject"></textarea>
					</div>
					<div class="col-12"></div>
						<label class="mt-3">Complaint Body</label>
						
						<textarea class="form-control" rows="10" placeholder="Complaint Body" name="complaint_body" required=""></textarea>
					</div>
					<div class="col-12 mt-5">
						<input type="submit" class="btn btn-outline-info p-2" value="Register Complaint">
						<input type="reset" class="btn btn-danger ml-5" value="Cancel">
					</div>
				</form>
	</div>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#msg').fadeOut(5000);
				var b = 'Parent' ;
				

				$.ajax({

					url: '<?= base_url()?>/Ajax/Parrent_Reciever_Id',
					type: 'post',
					data: {login_type:b},
					success:function(data)
					{
						$('#reciever_id').html(data);
					},
					error()
					{
						alert("Error");
					},

			});
		});
	</script>
</body>
</html>
