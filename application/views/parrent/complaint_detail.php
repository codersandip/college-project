<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background">
	<?php include 'header.php'; ?>
	<div class="container">
		<div class="col-md-8 offset-md-2 border mt-5 animated slideInUp">
			<h1 class="text-center pt-3">*  Complaint  *</h1>
			<hr>
			<div class="row">
				<div class="col-12 h3">
					<b>Complainter Name :- </b><i><?php echo $data->complainter_name ?></i>
				</div>
			</div>
			<hr class="my-4">
			<div class="row">
				<div class="col-4">
					<h3><b>Subject :-</b></h3>
				</div>
				<div class="col-8">
					<h5><?= $data->complaint_subject ?></h5>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-3 h3">
					<b>Body</b>
				</div>
				<div class="col-9">
					<h6><?= $data->complaint_body ; ?></h6>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-3 h3">
					<b>Complaint Send To :-</b>
				</div>
				<div class="col-9">


					<?php $data1 = $this->db->where('id',$data->reciever_id)->get('user_list')->row();
					?>
				<div style="font-size: 1.2rem">	
					<b>Name :- </b><?= $data1->first_name.'   '.$data1->last_name ?><br>
					<b>Destination :-</b><?= $data1->login_type ?><br>
					<b>Branch :-</b><?= $data1->branch ?><br>
					<b>Email :-</b><?= $data1->email ?>
				</div>
				</div>
			</div>


		</div>
	</div>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>