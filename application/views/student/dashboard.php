<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.remove
		{
			background-color: #ff0000;
			color: #FFF;
		}
		.read
		{
			background-color: darkgray;
		}
	</style>
</head>
<body class="background3">
	<?php include 'header.php'; ?>
	<div class="container mt-5">
		<h1 class="text-center">Complaint</h1>
		<div class="d-flex justify-content-end animated slideInLeft slow">
			<button class="btn btn-info w-auto p-2" id="complaintregister"> <span class="fa fa-plus-circle"></span> Register Complaint</button>
		</div>
		<?= $this->session->flashdata('msg'); ?>
		<table class="table text-center table-bordered mt-4">
			<thead>
				<tr class="table-light wow normal bounce">
					<th width="10mm"><button class="btn btn-danger btn-sm delete" id="delete"> <span class="fa fa-trash"></span> Delete</button></th>
					<th>Complaint Title</th>
					<th>Date Of Register</th>
					<th>View</th>
				</tr>
			</thead>
				<tbody>
				<?php foreach ($complaint as $key) {
					$col = $key->status;
					if (!$col) {
						$class = "read";
					}
					else
					{
						$class = "";
					}					
					?>
				<tr class="<?= $class; ?>">
					<th><input type="checkbox" class="delete_checkbox" value="<?= $key->id; ?>"></th>
					<td><?= $key->complaint_subject ; ?></td>
					<td><?= $key->register_date ; ?></td>
					<td><button class="btn btn-light" id="complaint" onclick="viewComplaint(<?= $key->id ?>)">View Complaint</button></td>
				</tr>

					<?php
					} ?>	
			</tbody>
		</table>
				<div class="d-flex justify-content-end">
					<?= $this->pagination->create_links(); ?>
				</div>




		<div class="modal fade" id="complaint_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title strong">Complaint</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	
      	<div id="regi_date" class="text-right"></div>
        <h3 class="text-secondary">Complaint Title </h3>
        <div id="com_title">
        	
        </div>

        <span class="my-5"><hr></span>
        <h3 class="text-secondary">Complaint Body</h3>
        <div id="complaint_body"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning	 w-25" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


	</div>
<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
		
</body>
<script type="text/javascript">
		$('#complaintregister').click(function(){
			window.open('<?= base_url() ?>Student/ComplaintRegister','_self');
		});

		function viewComplaint(id)
		{
			$.ajax({
				url:'<?= base_url() ?>Home/Complaint',
				type : 'post',
				data :{ id:id },
				success:function(data)
				{
					var data1 = JSON.parse(data);
					$("#com_title").html(data1.complaint_subject);
					$("#regi_date").html("<h5>Registration Date :-</h5>" + data1.register_date);
					$("#complaint_body").html(data1.complaint_body);
					$("#complaint_modal").modal('show');
				}
				
			});
		}

$('document').ready(function(){
			$('#msg').fadeOut(5000);
		$('.delete_checkbox').click(function(){
			if($(this).is(':checked')) {
				$(this).closest('th').addClass('remove');
			}
			else{
				$(this).closest('th').removeClass('remove');
			}
		});


		$('#delete').click(function(){

				var checkbox = $('.delete_checkbox:checked');
				if (checkbox.length > 0) {

					var checkbox_value = [];
					$(checkbox).each(function(){
						checkbox_value.push($(this).val());
					});

					$.ajax({
						url : '<?= base_url('Ajax/Delete_complaint')?>',
						type :'post',
						data : { id :checkbox_value },
						success:function(data){
							$('.remove').fadeOut(1000);
						},
						error:function(){
							alert('data');

						}
					});

				} else {

					alert('Select at least one checkbox');
				}
		});
	});


		// body...
</script>
</html>