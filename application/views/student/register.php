<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background2">
	<?php include 'header.php';?>
	<div class="container">
		<h1 align="center" class="mt-4">Student Registration</h1>
		<div class="row mt-5 mb-5">
			<?= $this->session->flashdata('msg'); ?>
			<div class="col-xl-5 col-lg-5 col-md-7 col-sm-7 offset-md-5 offset-lg-7 offset-xl-7">
				<form action="<?= base_url() ?>Student/RegisterValidate" method="post">
					<input type="hidden"class="form-control" name="login_type" value="Student">
					<label class="mt-2">First Name :</label>
					<input type="text"class="form-control" placeholder="First Name" name="first_name" value="<?= set_value('first_name')?>">
					<?= form_error('first_name');?>
					<label class="mt-2">Last Name :</label>
					<input type="text"class="form-control" placeholder="Last Name" name="last_name" value="<?= set_value('last_name')?>">
					<?= form_error('last_name');?>
					<label class="mt-2">Email :</label>
					<input type="text"class="form-control" placeholder="Email" name="email"  value="<?= set_value('email')?>">
					<?= form_error('email');?>
					<label class="mt-2">Contact No. :</label>
					<input type="text"class="form-control" placeholder="Contact No." name="mobile_no" value="<?= set_value('mobile_no')?>">
					<?= form_error('mobile_no');?>
					<label class="mt-2">Enrollment No. :</label>
					<input type="text"class="form-control" placeholder="Enrollment No." name="enrollment_no" value="<?= set_value('enrollment_no')?>">
					<?= form_error('enrollment_no');?>
					<label class="mt-2">Branch</label>
						<select class="custom-select" name="branch"  value="<?= set_value('branch')?>">
							<option value="">Select Branch</option>
							<option value="Computer Engg">Computer Engg</option>
							<option value="Mechanical Engg">Mechanical Engg</option>
							<option value="Civil Engg">Civil Engg</option>
							<option value="Electronic And Telecomunication Engg">Electronic And Telecomunication Engg</option>
						</select>
					<?= form_error('branch');?>
					<label class="mt-2">Password</label>
					<input type="password"class="form-control" placeholder="Password" name="password" value="<?= set_value('password')?>">
					<?= form_error('password');?>
					<label class="mt-2">Confirm Password</label>
					<input type="password"class="form-control" placeholder="Confirm Password" name="confirm_password" value="<?= set_value('confirm_password')?>">
					<?= form_error('confirm_password');?>
					<input type="submit" value="Register" class="mt-4 w-25 btn btn-outline-primary">
					<input type="" value="Cancel" class="btn btn-danger mt-4 ml-5 w-25 ">
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#msg').fadeOut(5000);
		});
	</script>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>