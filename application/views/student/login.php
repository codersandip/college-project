<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background3">
	<?php include 'header.php'; ?>
	<div class="container">
		<div class="row mt-5">
			<div class="background7 col-xl-4 p-3 col-lg-4 col-md-6 col-sm-8 offset-xl-4 border offset-lg-4 offset-md-3 rounded border-dark offset-sm-2 zoomInDown wow">
				<div class="text-center text-dark font-italic font-weight-bold bg-default" style="font-size: 35px">
					 Login
				</div>
				<hr>
				<?= $this->session->flashdata('msg'); ?>
				<div>
					<?= form_open('Student/Login')?> <span class="fa fa-user"></span> <?= form_label('Email :').form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Email','value'=>set_value('email')]).form_error('email').br()?> <span class="fa fa-lock"></span> <?=form_label('Password :').form_password(['name'=>'password','class'=>'form-control','placeholder'=>'Password','value'=>set_value('password')]).form_error('password').br() ?>
					<div class="text-center">
						<?= form_submit('','submit',['class'=>'btn btn-outline-primary']).nbs(10).form_reset('','Reset',['class'=>'btn btn-danger']) ?>
					</div>
					<hr>
					<div>
						Forgot <?= anchor('Home/Forgot','Password?'). nbs(15); ?> <a href="<?= base_url() ?>Student/Register">Create New User</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#msg').fadeOut(5000);
		});
	</script>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>