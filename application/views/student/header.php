    <div style="font-size: 45px;" class="background1 py-4 animated slideInDown bg-light text-center">
        Grivience Redressal Portal
      </div>
  <div class="sticky-top background1">
    <nav class="navbar navbar-expand-lg navbar-dark container animated slideInUp">
      <a class="navbar-brand" href="<?= base_url() ?>"><span class="fa fa-home" style="font-size: 1.5rem"></span> Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <?php if($this->session->userdata('id') != null) { ?>
            <div class="collapse navbar-collapse" id="navbarColor01">
              <span class="mr-auto"></span>
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href=""> <span class="sr-only">(current)</span></a>
                </li>
                 <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span>&nbsp;&nbsp;<?= $this->session->userdata('first_name')."  ".$this->session->userdata('last_name') ?></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?= base_url();?>"><span class="fa fa-edit fa-1x"></span> Edit Profile</a>
                <a class="dropdown-item" href="<?= base_url('Home/Password');?>"><span class="fa fa-user-secret"></span> Change Password</a>
                <a class="dropdown-item" href="<?= base_url('Home/Logout');?>"><span class="fa fa-sign-out"></span> Logout</a>
              </div>
                </li>
              </ul>
            </div>

          <?php } ?>
    </nav>
  </div>