<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.remove
		{
			background-color: #ff0000;
			color: #FFF;
		}
		.read
		{
			background-color: darkgray;
		}
	</style>
</head>
<body class="background3">
	<?php include 'header.php'; ?>
	<div class="container mt-5">
		<?= $this->session->flashdata('msg'); ?>
		<h1 class="text-center">Complaint List</h1>
	<table class="table text-center table-bordered mt-4">
			<thead>
				<tr class="table-light wow normal bounce">
					<th width="10mm"><button class="btn btn-danger btn-sm delete" id="delete"> <span class="fa fa-trash"></span> Delete</button></th>
					<th>Complaint Title</th>
					<th>Date Of Register</th>
					<th>View</th>
				</tr>
			</thead>
				<tbody>
				<?php   foreach ($complaint as $key) {
					$col = $key->status;
					$class = '';
					if (!$col) {
						$class = "read";
					}
								
					?>
				<tr class="<?= $class; ?> animated normal headShake ">
					<th><input type="checkbox" class="delete_checkbox" value="<?= $key->id; ?>"></th>
					<td><?= $key->complaint_subject .'			'.$col; ?></td>
					<td><?= $key->register_date ; ?></td>
					<td><button class="btn btn-outline-dark" id="complaint" onclick="window.open('<?= base_url() ?>Teacher/complaint_view/<?= $key->id.'/'.$key->complainter_id ; ?>','_SELF')">View Complaint</button></td>
				</tr>

					<?php
					} ?>	
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
				<div class="d-flex justify-content-end">
					<?= $this->pagination->create_links(); ?>
				</div>
						
					</td>
				</tr>
			</tfoot>			
		</table>


	</div>
<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
<script type="text/javascript">
	$('document').ready(function(){
			$('.delete_checkbox').click(function(){
			if($(this).is(':checked')) {

				$(this).closest('th').addClass('remove');
				$(this).closest('tr').addClass('remov');
			}
			else{
				$(this).closest('th').removeClass('remove');
				$(this).closest('tr').removeClass('remov');

			}
		});


		$('#delete').click(function(){
			$('#msg').fadeOut(5000);

				var checkbox = $('.delete_checkbox:checked');
				if (checkbox.length > 0) {

					var checkbox_value = [];
					$(checkbox).each(function(){
						checkbox_value.push($(this).val());
					});

					$.ajax({
						url : '<?= base_url('Ajax/Delete_complaint')?>',
						type :'post',
						data : { id :checkbox_value },
						success:function(data){
							$('.remov').fadeOut(1000);
						},
						error:function(){
							alert('data');

						}
					});

				} else {

					alert('Select at least one checkbox');
				}
		});
	});
</script>
</html>
