<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php include 'header.php'; ?>
	<div class="modal animated fade" id="password_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
        	<?=  form_open('Home/ChangePassword') . form_label('Enter New Password :- ') . form_password(['name'=>'password','class'=>'form-control','placeholder'=>'New Password','autofocus'=>'','value'=>set_value('password')]).form_error('password') . br().form_label('Enter Confirm Password :- ').form_password(['name'=>'con_password','class'=>'form-control','placeholder'=>'Confirm Password','value'=>set_value('con_password')]).form_error('con_password')  ?>
        	<div class="d-flex justify-content-end mt-3">
        		<?= form_submit('','Change Password',['class'=>'btn btn-success']).form_close() ?>
        		<a href="#" class="btn btn-danger mx-3" id="dsd">Close</a>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
	$('document').ready(function(){
		$('#password_modal').modal('show');
		$('#dsd').click(function(){

			$('#password_modal').modal('hide');
	
			window.open('<?= base_url() ?>','_SELF');
	
		});
	});
</script>
</body>
</html>