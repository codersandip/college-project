<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background4">
	<?php include 'header.php'; ?>
	<div class="container mt-5">
		<div class="row">
			<?php if(!isset($email)){ ?>
			<div class="col-md-4 border offset-md-4">
			<?= $this->session->flashdata('msg'); ?>
				<h2 class="text-center">Forgot Password</h2>
				<hr>
				<div class="form-group">
					<form action="<?= base_url('Home/Forgot_veri') ?>" method="post">
						<div class="form-group">
							<label for="email">Enter your Email Address :-</label>
							<input type="email" name="email" class="form-control" placeholder="Email" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn"><span class="fa fa-search"></span> Search</button>
						</div>
					</form>
				</div>
			</div>
		<?php } else{ ?>

			<div class="col-md-4 offset-md-4 border">
				<h3 class="text-center my-3">Enter OTP</h3>
				<label>The OTP will be send on your Email Address  <span class="text-info h5" style="text-decoration: underline;">'<?php if(isset($email)){ echo $email['email']; } ?>'</span></label>
				<br>
				OTP Will be valid up to 5 min.
				<br><br>
				<form action="<?= base_url('Home/Verify_otp') ?>" method="post">
					<input type="hidden" name="email" value="<?php if(isset($email)){ echo $email['email']; } ?>">
					Enter OTP :-
					<input type="text" name="otp" class="form-control mt-2" placeholder="OTP">
					<br>
					<div align="center">
						<input type="submit" value="Verify OTP" class="btn btn-primary">
					</div>
				</form>
			</div>
		<?php } ?>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#msg').fadeOut(5000);
		});
	</script>
	<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>