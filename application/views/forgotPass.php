<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="background4">
	<?php include 'header.php'; ?>
	<div class="container mt-5">
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<form action="<?= base_url('Home/Change_Password') ?>" method="post">
						Enter new Password :-
						<input type="hidden" name="email" value="<?= $mail ?>">
						<input type="password" class="form-control" name="password" placeholder="Password" value="<?= set_value('password') ?>">
						<?= form_error('password') ?>
						<br>
						Re-enter the Password :-
						<input type="password" name="con_password" class="form-control" placeholder="Re-enter Password" value="<?= set_value('con_password') ?>">
						<?= form_error('con_password') ?>

						<br>
						<input type="submit" value="Change Password" class="btn w-50">
					</form>
				</div>
			</div>
		</div>
		<div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
</body>
</html>