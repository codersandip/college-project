<?php defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
  <style type="text/css">
    .text
    {
     color:  #EB1212;
     font-weight: 450;
     text-align: center;
     text-decoration: none;
    }
    .text:hover
    {
      color: #FFA600;
     text-decoration: none;
    }
  </style>
</head>
<body class="background">
	<?php include 'header.php'; ?>
	<div class="offset-md-3 col-md-6">
    <?= $this->session->flashdata('msg') ?> 
  </div>
	<div class="row text-center">
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 p-5">
      <a href="<?= base_url('Administrator') ?>">
        <div class="bg-danger rounded-circle h1 background7" style="height: 40%">
        <div class="p-5 text-center text">
          Admin
          <br>
          <img src="<?= base_url() ?>images/admin.png">
        </div>  
        </div>
      </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 p-5">
      <a href="<?= base_url('Teacher') ?>">
        <div class="bg-danger rounded-circle h1 background7" style="height: 40%">
        <div class="p-5 text-center text">
          Teacher
          <br>
          <img src="<?= base_url() ?>images/faculty.png">
        </div>  
        </div>
      </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 p-5">
      <a href="<?= base_url('Parrent') ?>">
        <div class="bg-danger rounded-circle h1 background7" style="height: 40%">
        <div class="p-5 text-center text">
          Parent
          <br>
          <img src="<?= base_url() ?>images/parent.png">
        </div>  
        </div>
      </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 p-5">
      <a href="<?= base_url('Student') ?>">
        <div class="bg-danger rounded-circle h1 background7" style="height: 40%">
        <div class="p-5 text-center text">
          Student
          <br>
          <img src="<?= base_url() ?>images/student.png">
        </div>  
        </div>
      </a>
    </div>

    <div class="container">
      
    <?php $this->load->view('footer'); ?>
    </div>
  </div>
	<script type="text/javascript">
		$('document').ready(function(){
			$('#msg').fadeOut(5000);
		});
	</script>
</body>
</html>