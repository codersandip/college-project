<?php
$config = array(
	'login'=> array(
			array(
				'field'=>'email',
				'label'=>'Email',
				'rules'=>'required|valid_email',
			),
			array(
				'field'=>'password',
				'label'=>'Password',
				'rules'=>'required',
			),
	),


	'studentRegister'=>array(
			array(
				'field'=>'first_name',
				'label'=>'First Name',
				'rules'=>'required|alpha',
			),
			array(
				'field'=>'last_name',
				'label'=>'Last Name',
				'rules'=>'required|alpha',
			),
			array(
				'field'=>'email',
				'label'=>'Email',
				'rules'=>'required|valid_email|is_unique[user_list.email]',
			),
			array(
				'field'=>'mobile_no',
				'label'=>'Contact No.',
				'rules'=>'required|numeric|exact_length[10]',
			),
			array(
				'field'=>'enrollment_no',
				'label'=>'Enrollment No.',
				'rules'=>'required|numeric',
			),
			array(
				'field'=>'branch',
				'label'=>'Branch',
				'rules'=>'required',
			),
			array(
				'field'=>'password',
				'label'=>'Password',
				'rules'=>'required|min_length[8]',
			),
			array(
				'field'=>'confirm_password',
				'label'=>'Confirm Password',
				'rules'=>'required|matches[password]',
			),


	),

		'complaint_register' => array(
				'reciever_id'=>'required',
				'complaint_subject'=>'required',
				'complaint_body'=>'required',
		),


);



$config['error_prefix'] = '<div style="color:red;">*';

$config['error_suffix'] = '</div>';