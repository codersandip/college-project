<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->view('link');
		$this->demo->student_session();
		$this->demo->Teacher_session();
		$this->demo->Parrent_session();

	}
	public function index()
	{
		$this->demo->Administrator_session();

		$this->load->view('Administrator/login');
	}

	public function login()
	{
		$post = $this->input->post();

		if($this->form_validation->run('login'))
		{
			if($data = $this->demo->login($post))
			{
			$session = 
			array(
				'id'=>$data->id,
				'first_name'=>$data->first_name,
				'last_name'=>$data->last_name,
				'email'=>$data->email,
				'mobile_no'=>$data->mobile_no,
				'login_type'=>$data->login_type,
				'branch'=>$data->branch,
				//'enrollment_no'=>$data->enrollment_no
			);
			
				$this->session->set_userdata($session);
				return redirect('Administrator/Dashboard');
			}
			else
			{
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Invalid</strong>   <a href="" class="alert-link">  Email/Password</a>.
						</div>');
				return redirect('Administrator');
			}
		}
		else
		{
			$this->load->view('Administrator/login');
		}
	}


		public function dashboard()
	{
		$this->demo->session();
$config=[
					'base_url'=>base_url('Administrator/Dashboard'),
					'per_page'=>10,
					'total_rows'=>$this->demo->complaint_views_row(),
					/*'suffix' => '.jsp',*/
			 		'full_tag_open'=>'<ul class="pagination">',
					'full_tag_close'=>'</ul>',
					
					'next_tag_open'=>'<li class="page-link">',
					'next_tag_close'=>'</li>',
					
					'last_tag_open'=>'<li class="page-link">',
					'last_tag_close'=>'</li>',

					'first_tag_open'=>'<li class="page-link">',
					'first_tag_close'=>'</li>',

					'prev_tag_open'=>'<li class="page-link">',
					'prev_tag_close'=>'</li>',

					'num_tag_open'=>'<li class="page-link">',
					'num_tag_close'=>'</li>',

					'cur_tag_open'=>'<li class="page-link active bg-primary text-light"><a>',
					'cur_tag_close'=>'</a></li>',
				];
				$this->pagination->initialize($config);

		$this->demo->session();
		$data = $this->demo->complaint_views($config['per_page'],$this->uri->segment(3));
		$this->load->view('Administrator/dashboard',['complaint'=>$data]);
	}

	public function complaint_view($id,$i)

	{
		$this->demo->session();

		$data = $this->demo->complaint_view_update($id);

		$data1 = $this->demo->user_info($i);
		$data1 = $this->demo->user_info($i);
$body = '<h2>Your Complaint is viewed</h2><br><b>at :-</b>'.date('d/m/Y h:i:s A').'<h2>Complaint Subject :-</h2>'.$data->complaint_subject;
			if($this->demo->mails($data1->email,'Complaint View',$body))
			{
				$this->load->view('Administrator/complaint_detail',['data'=>$data,'data1'=>$data1]);
			}
	}

	public function profile()
	{
		$this->demo->session();

		$data = $this->demo->user_info($this->session->userdata('id'));

		$this->load->view('Administrator/profile',compact('data'));
	}


	public function profile_validate()
	{
		
		$this->demo->session();

		$this->form_validation->set_rules('first_name','First Name','required|alpha');
		$this->form_validation->set_rules('last_name','Last Name','required|alpha');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('mobile_no','Mobile No.','required|exact_length[10]|numeric');
		
$post = $this->input->post();
		if($this->form_validation->run())
		{
			$post = [
						'first_name'=>$post['first_name'],
						'last_name'=>$post['last_name'],
						'mobile_no'=>$post['mobile_no'],
						'email'=>$post['email']
					];
					$to = $post['email'];
					$subject = 'Profile Updated';
					$body = '
						<h1>The Profile Updated</h1><br><h2>with following credential</h2><br>
						<b>First Name :-</b>'.$post['first_name'].
						'<br><b>Last Name :-</b>'.$post['last_name'].
						'<br><b>Email :-</b>'.$post['email'].
						'<br><b>Mobile No. :-</b>'.$post['mobile_no'].'<h1 style="text-align:center">Thank You</h1>'
					;


					$this->demo->mails($to,$subject,$body);
					if($this->demo->update_profile($post))
					{
						$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Profile</strong>   <a href="" class="alert-link">Updated</a>.
						</div>');
			$body = 'The profile is be Updated';
						
			$this->demo->mails($post['email'],'Profile Updated',$body);

						return redirect('Administrator/Dashboard');
					}
		}
		else
		{
			$this->profile();

		}
	}

	public function teacher()
	{
		$this->demo->session();

		$this->load->view('Administrator/teacher');
	}

	public function teacherv()
	{
		$this->demo->session();

		$post = $this->input->post();
		$this->form_validation->set_rules('first_name','First Name','required|alpha');
		$this->form_validation->set_rules('last_name','Last Name','required|alpha');
		$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[user_list.email]');
		$this->form_validation->set_rules('mobile_no','Mobile No.','required|numeric');
		$this->form_validation->set_rules('branch','Branch','required');
		if($this->form_validation->run()):
			$body = '<h1>Teacher Registration</h1><br><b>First Name :- </b>'.$post['first_name'].'<br><b>Last Name :- </b>'.$post['last_name'].'<br><b>Email :- </b>'.$post['email'].'<h1 align="center">Thank You</h1>';

// echo $body ;
// exit;
			$this->demo->mails($post['email'],'Registration',$body);
			if($this->demo->register($post))
			{
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Teacher </strong>   <a href="" class="alert-link"> Successfully Registered</a>.
						</div>');
				return redirect('Administrator');
			}
			else
			{
				echo "ww";
			}

		else:
			$this->load->view('Administrator/teacher');
		endif;

	}

	public function user()
	{
		$this->demo->session();

		$data = $this->demo->user_list();
		$this->load->view('Administrator/user',['data'=> $data]);
	}

	public function user_update()
	{
		$this->demo->session();

		$post = $this->input->post();
		// print_r($post);
		// echo count($post['first_name']);
		for ($i=0; $i <count($post['first_name']) ; $i++) { 
			// echo $i.'<br>';
			$post1 =[
				'first_name'=>$post['first_name'][$i],
				'last_name'=>$post['last_name'][$i],
				'mobile_no'=>$post['mobile_no'][$i],
				'email'=>$post['email'][$i],
				'enrollment_no'=>$post['enrollment_no'][$i],
				'branch'=>$post['branch'][$i],
				'login_type'=>$post['login_type'][$i]
			];
			$post2 = ['id'=>$post['id'][$i]];
			$this->db->where($post2)->update('user_list',$post1);
			$body = 'The profile is be Updated';
			$this->demo->mails($post1['email'],'Profile Updated',$body);
		}
		return redirect('Administrator/User');
	}
}