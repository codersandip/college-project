<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		// $this->load->view('link');
		

	}
	public function index()
	{
		$this->load->view('link');
		$this->load->view('home');
		$this->demo->student_session();
		$this->demo->Teacher_session();
		$this->demo->Parrent_session();
		$this->demo->Administrator_session();
	}


	public function logout()
	{
		$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Logout</strong>   <a href="" class="alert-link">  Succesfully</a>.
						</div>');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('first_name');
		$this->session->unset_userdata('last_name');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('mobile_no');
		$this->session->unset_userdata('login_type');
		return redirect(base_url());
	}

	public function complaint()
	{
		$id = $this->input->post();
		$data = $this->db->where($id)->get('complaint')->row();
		echo json_encode($data);
	}

	public function complaintReply()
	{
		$post = $this->input->post();
$body = '<h1 style="color:blue">Complaint Reply</h1><br><h3 style="color:blue">Complaint Title :- </h3><h4>'.$post['title'].'</h4><h3 style="color:blue">Complaint Reply :- </h3><h4>'.$post['reply'].'</h4>';

	
		if($this->demo->mails($post['email'],'Complaint Reply',$body)){
			redirect('Teacher/Dashboard');
		}
	}

	public function password()
	{
		$this->load->view('link');
		$this->load->view('Password');
	}

	public function changePassword()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[15]');
		$this->form_validation->set_rules('con_password','Confirm Password','required|matches[password]');
		

		if($this->form_validation->run())
		{
			print_r($this->session->userdata());
			$body = 'The Passowrd Has been changd<br><h1>'.$post['password'].'</h1>';
			$this->demo->mails($this->session->userdata('email'),'Password Changed',$body);
				// exit;		
			if($this->demo->changePassword($post['password']))
			{
				$login_type = $this->session->userdata('login_type');
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Password</strong>   <a href="" class="alert-link">Change Succesfully</a>.
						</div>');
				print_r($this->session->userdata());
				if ($login_type =="Teacher") {
					return redirect('Teacher/Dashboard');
				}

				if ($login_type =="Student") {
					return redirect('Student/Dashboard');
				}

				if ($login_type =="Parrent") {
					return redirect('Parent/Dashboard');
				}

				if ($login_type =="Administrator") {
					return redirect('Administrator/Dashboard');
				}
				if ($login_type =="Principle") {
					return redirect('Administrator/Dashboard');
				}
			}
		}
		else
		{
		$this->load->view('link');
		$this->load->view('Password');
		}
	}

	public function forgot()
	{
		$this->load->view('link');
		$this->load->view('forgot');

	}

	public function Forgot_veri()
	{
		$this->load->view('link');
		$post = $this->input->post();
		$data = $this->db->where($post)->get('user_list')->num_rows();
		if ($data > 0) {
			$otp = rand(100000,999999);
			
			$to = $post['email'];
			$subject = "OTP Reset Password";
			$body = 'OTP for reset password<br><br><h1>'.$otp.'</h1>';

			if($this->demo->mails($to,$subject,$body)){
				$this->session->set_userdata('otp',$otp);
				$email = ['email'=>$post['email'],'otp'=>$otp];
				$this->load->view('forgot',compact('email'));
			}
		}
		else
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger	" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Invalid</strong>   <a href="" class="alert-link">Email</a>.
						</div>');
			return redirect('Home/Forgot');
		}

	}

	public function verify_otp()
	{
		$this->load->view('link');
		$post = $this->input->post();
$mail =$post['email'];
echo $post['otp'].'     '.$this->session->userdata('otp');
		if ($post['otp'] == $this->session->userdata('otp')) {
				$this->load->view('forgotPass',compact('mail'));
		}
		else
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger	" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Invalid</strong>   <a href="" class="alert-link">OTP</a>.
						</div>');
			return redirect('Home/Forgot');
		}
	}

	public function change_Password()
	{
		$this->load->view('link');

		$post = $this->input->post();
		$this->form_validation->set_rules('password','Password','required|min_length[8]');
		$this->form_validation->set_rules('con_password','Re-enter Password','required|min_length[8]|matches[password]');
		if($this->form_validation->run())
		{
			if($this->db->where('email',$post['email'])->update('user_list',['password'=>$post['password']]))
			{
				echo "string";
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Password</strong>   <a href="" class="alert-link">Change Succesfully</a>.
						</div>');
				return redirect('Student');
			}
		}
		else
		{
				$mail =$post['email'];
				$this->load->view('forgotPass',compact('mail'));
		}
	}

}