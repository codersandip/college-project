<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->view('link');
		$this->demo->student_session();
		$this->demo->Parrent_session();
		$this->demo->Administrator_session();
	}
	public function index()
	{
		$this->demo->Teacher_session();
		
		$this->load->view('Teacher/Login');
	}

	public function login()
	{
		$post = $this->input->post();
		if($this->form_validation->run('login'))
		{
			if($data = $this->demo->login($post))
			{
			$session = 
			array(
				'id'=>$data->id,
				'first_name'=>$data->first_name,
				'last_name'=>$data->last_name,
				'email'=>$data->email,
				'mobile_no'=>$data->mobile_no,
				'login_type'=>$data->login_type,
				'branch'=>$data->branch,
				//'enrollment_no'=>$data->enrollment_no
			);
			
				$this->session->set_userdata($session);
				return redirect('Teacher/Dashboard');
			}
			else
			{
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Invalid</strong>   <a href="" class="alert-link">  Email/Password</a>.
						</div>');
				return redirect('Teacher');
			}
		}
		else
		{
			$this->load->view('teacher/login');
		}
	}

		public function dashboard()
	{
		$this->demo->session();
$config=[
					'base_url'=>base_url('Teacher/Dashboard'),
					'per_page'=>10,
					'total_rows'=>$this->demo->complaint_views_row(),
					/*'suffix' => '.jsp',*/
			 		'full_tag_open'=>'<ul class="pagination">',
					'full_tag_close'=>'</ul>',
					'next_tag_open'=>'<li class="page-link">',
					'next_tag_close'=>'</li>',
					
					'last_tag_open'=>'<li class="page-link">',
					'last_tag_close'=>'</li>',

					'first_tag_open'=>'<li class="page-link">',
					'first_tag_close'=>'</li>',

					'prev_tag_open'=>'<li class="page-link">',
					'prev_tag_close'=>'</li>',
					'num_tag_open'=>'<li class="page-link">',
					'num_tag_close'=>'</li>',
					'cur_tag_open'=>'<li class="page-link active bg-primary text-light"><a>',
					'cur_tag_close'=>'</a></li>',
				];
				$this->pagination->initialize($config);

		$this->demo->session();
		$data = $this->demo->complaint_views($config['per_page'],$this->uri->segment(3));
		$this->load->view('teacher/dashboard',['complaint'=>$data]);
	}

	public function complaint_view($id,$i)
	{

		$this->demo->session();

		$data = $this->demo->complaint_view_update($id);

		$data1 = $this->demo->user_info($i);
$body = '<h2>Your Complaint is viewed</h2><br><b>at :-</b>'.date('d/m/Y h:i:s A').'<h2>Complaint Subject :-</h2>'.$data->complaint_subject;
			if($this->demo->mails($data1->email,'Complaint View',$body))
			{
				$this->load->view('Teacher/complaint_detail',['data'=>$data,'data1'=>$data1]);
			}
	}

	public function profile()
	{
		$data = $this->demo->user_info($this->session->userdata('id'));

		$this->load->view('teacher/profile',compact('data'));
	}


	public function profile_validate()
	{
	
		$this->demo->session();

		$this->form_validation->set_rules('first_name','First Name','required|alpha');
		$this->form_validation->set_rules('last_name','Last Name','required|alpha');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('mobile_no','Mobile No.','required|exact_length[10]|numeric');
		$this->form_validation->set_rules('branch','Branch','required');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric|min_length[8]');
		$this->form_validation->set_rules('con_password','Confirm Password','required|matches[password]');
	

		if($this->form_validation->run())
		{
			$post = [
						'first_name'=>$post['first_name'],
						'last_name'=>$post['last_name'],
						'mobile_no'=>$post['mobile_no'],
						'email'=>$post['email'],
						'password'=>$post['password'],
						'branch'=>$post['branch']
					];

					if($this->demo->update_profile($post))
					{
						$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Profile</strong>   <a href="" class="alert-link">Updated</a>.
						</div>');
						return redirect('Teacher/Dashboard');
					}
		}
		else
		{
			$this->profile();

		}
	}
}