<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parrent extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->view('link');
		$this->demo->student_session();
		$this->demo->Teacher_session();
		$this->demo->Administrator_session();
	}
	public function index()
	{
		$this->demo->Parrent_session();
		$this->load->view('parrent/login');
	}

	public function register()
	{
		$this->load->view('parrent/register');
	}

	public function RegisterValidate()
	{
		$post = $this->input->post();
				if($this->form_validation->run('studentRegister'))
		{
			$this->demo->mails($post['email'],'Parrent Registration','The registration has been Successfully');
		$post1 = array('first_name'=>$post['first_name'],'last_name'=>$post['last_name'],'email'=>$post['email'],'mobile_no'=>$post['mobile_no'],'enrollment_no'=>$post['enrollment_no'],'branch'=>$post['branch'],'password'=>$post['password'],'login_type'=>$post['login_type']);
		if($this->demo->register($post1)){
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Parrent</strong>   <a href="" class="alert-link">Registered Successfully</a>.
						</div>');
				return redirect('Parrent');
		}
		}
		else
		{
			$this->load->view('parrent/register');		
		}

	}

	public function login()
	{
		$post = $this->input->post();
		if($this->form_validation->run('login'))
		{
			if($data = $this->demo->login($post))
			{
			$session = 
			array(
				'id'=>$data->id,
				'first_name'=>$data->first_name,
				'last_name'=>$data->last_name,
				'email'=>$data->email,
				'mobile_no'=>$data->mobile_no,
				'login_type'=>$data->login_type,
				'branch'=>$data->branch,
				'enrollment_no'=>$data->enrollment_no
			);
			
				$this->session->set_userdata($session);
				return redirect('Parrent/Dashboard');
			}
			else
			{
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Invalid</strong>   <a href="" class="alert-link">  Email/Password</a>.
						</div>');
				return redirect('Parrent');
			}
		}
		else
		{
			$this->load->view('Parrent/login');
		}
	}

	public function dashboard()
	{
		
		// $this->load->view('parrent/dashboard');
			$this->demo->session();
$config=[
					'base_url'=>base_url('Parrent/Dashboard'),
					'per_page'=>10,
					'total_rows'=>$this->demo->complaint_views_row(),
					/*'suffix' => '.jsp',*/
			 		'full_tag_open'=>'<ul class="pagination">',
					'full_tag_close'=>'</ul>',
					'next_tag_open'=>'<li class="page-link">',
					'next_tag_close'=>'</li>',
					
					'last_tag_open'=>'<li class="page-link">',
					'last_tag_close'=>'</li>',

					'first_tag_open'=>'<li class="page-link">',
					'first_tag_close'=>'</li>',

					'prev_tag_open'=>'<li class="page-link">',
					'prev_tag_close'=>'</li>',
					'num_tag_open'=>'<li class="page-link">',
					'num_tag_close'=>'</li>',
					'cur_tag_open'=>'<li class="page-link active bg-primary text-light"><a>',
					'cur_tag_close'=>'</a></li>',
				];
				$this->pagination->initialize($config);

		$this->demo->session();
		$data = $this->demo->complaint_view($config['per_page'],$this->uri->segment(3));
		$this->load->view('parrent/dashboard',['complaint'=>$data]);
	}

	public function complaintRegister()
	{
		$this->load->view('parrent/complaintRegister');
	}


		public function complaintRegisterValidate()
	{
				$this->demo->session();		
			$post = $this->input->post();
		

		$this->form_validation->set_rules('reciever_id','','trim|required');
		$this->form_validation->set_rules('complaint_subject','','trim|required');
		$this->form_validation->set_rules('complaint_body','','trim|required');



		if($this->form_validation->run())
		{
	
		$re_data = $this->demo->user_info($post['reciever_id']);
		$se_data = $this->demo->user_info($this->session->userdata('id'));

		$body = '<h1>Complaint Registration</h1><br><b> Name :- </b>'.$se_data->first_name.'  '.$se_data->last_name.'<br><b>Email :- </b>'.$se_data->email.'<br><b>Destination :-</b>'.$se_data->login_type.'<br><b>Branch :-</b>'.$se_data->branch.'<br><b>Complaint Subject :-</b><br><b>'.$post['complaint_subject'];
		

			if($this->demo->student_complaint($post) && $this->demo->mails($re_data->email,'Complaint Registration',$body) && $this->demo->mails($se_data->email,'Complaint Registration','<h2>Your Complaint has been Registered </h2><br><h4>Complaint Subject :-</h4>'.$post['complaint_subject']))
			{
				$this->session->set_flashdata('msg','<div class="alert alert-dismissible my-3 alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Complaint</strong> Registred Successfully
						</div>');
				return redirect('Parrent/Dashboard');
			}

		}
		else
		{
		$this->load->view('parrent/complaintRegister');
		}
	}


	public function complaint_view($id,$i)
	{

		$this->demo->session();

		$data = $this->demo->complaint_vie($id);

		$data1 = $this->demo->user_info($i);

		$this->load->view('Parrent/complaint_detail',['data'=>$data,'data1'=>$data1]);
	}



}