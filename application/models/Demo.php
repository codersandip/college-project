<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Demo extends CI_Model
{
	
	function __construct()
	{
		
		parent::__construct();
	}

	public function login($post)
	{
		return $this->db->where($post)->get('user_list')->row();
	}

	public function user_info($id)
	{
		return $this->db->where('id',$id)->get('user_list')->row();
	}

	public function session()
	{
		if(empty($this->session->userdata('id')))
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-danger" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Please </strong>   <a href="" class="alert-link"> Login</a>.
						</div>');
			return redirect('');
		}
	}

		public function Student_session()
	{
		if($this->session->userdata('login_type')=='Student')
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>You are </strong>   <a href="" class="alert-link">already Login</a>.
						</div>');
			return redirect('Student/Dashboard');
		}
	}

		public function Teacher_session()
	{
		if($this->session->userdata('login_type')=='Teacher')
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>You are </strong>   <a href="" class="alert-link">already Login</a>.
						</div>');
			return redirect('Teacher/Dashboard');
		}
	}

		public function Parrent_session()
	{
		if($this->session->userdata('login_type')=='Parent')
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>You are </strong>   <a href="" class="alert-link">already Login</a>.
						</div>');
			return redirect('Parrent/Dashboard');
		}
	}

		public function Administrator_session()
	{
		if($this->session->userdata('login_type')=='Administrator' OR $this->session->userdata('login_type')=='Principle')
		{
			$this->session->set_flashdata('msg','<div class="alert alert-dismissible alert-success" id="msg">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>You are </strong>   <a href="" class="alert-link">already Login</a>.
						</div>');
			return redirect('Administrator/Dashboard');
		}
	}


	public function student_complaint($post)
	{
		$post['register_date'] = date('d-M-Y h:i:s A');
		return $this->db->insert('complaint',$post);
	}



	public function complaint_view($limit,$offset)
	{
					
		return $this->db->where('complainter_id',$this->session->userdata('id'))->limit($limit,$offset)->get('complaint')->result();
	}

	public function complaint_view_row()
	{
		return $this->db->where('complainter_id',$this->session->userdata('id'))
					->get('complaint')
					->num_rows();
	}



	public function complaint_views($limit,$offset)
	{
					
		return $this->db->where('reciever_id',$this->session->userdata('id'))->limit($limit,$offset)->get('complaint')->result();
	}

	public function complaint_views_row()
	{
		return $this->db->where('reciever_id',$this->session->userdata('id'))
					->get('complaint')
					->num_rows();
	}





	public function parrent_Reciever()
	{
		return $this->db->not_like('login_type','Student')->not_like('login_type','Parent')->get('user_list')->result();
	}




	public function complaint_view_update($id)
	{
		$this->db->where('id',$id)->update('complaint',['status'=>1]);

		return $this->db->where('id',$id)->get('complaint')->row();

	}

	public function complaint_vie($id)
	{
		return $this->db->where('id',$id)->get('complaint')->row();
	}

	public function changePassword($post)
	{
		return $this->db->where('id',$this->session->userdata('id'))->update('user_list',['password'=>$post]);
	}

	public function update_profile($post)
	{
		return $this->db->where('id',$this->session->userdata('id'))->update('user_list',$post);
	}

	public function register($post)
	{
		// print_r($post);
		return $this->db->insert('user_list', $post);
	}

	public function user_list()
	{
		return $this->db->where('login_type !=', 'Administrator')
						 ->where('login_type !=', 'Principle')
						 ->get('user_list')
						 ->result();
	}

	public function user_update($post,$post1)
	{
		return $this->db->where($post1)->update('user_list',$post);
	}

	public function mails($to,$subject,$body)
	{
	// 		$this->load->library("PhpMailerLib");
		      $mail = $this->phpmailerlib->load();
	try {
		    //Server settings
		    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'sandiptawhare18@gmail.com';                 // SMTP username
		    $mail->Password = '9527849688';                           // SMTP password
		    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465;                                    // TCP port to connect to
		    //Recipients
		    $mail->setFrom('sandiptawhare18@gmail.com', 'Grivience Redressal Portal');
		    $mail->addAddress($to);     // Add a recipient

		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $body;
		   // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		   return $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
	}


}
