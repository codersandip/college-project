-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2019 at 06:31 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--
CREATE DATABASE IF NOT EXISTS `project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `project`;

-- --------------------------------------------------------

--
-- Table structure for table `complaint`
--

CREATE TABLE `complaint` (
  `id` int(11) NOT NULL,
  `complainter_id` int(255) DEFAULT NULL,
  `complainter_name` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `enrollment_no` varchar(255) NOT NULL,
  `reciever_id` int(255) NOT NULL,
  `complaint_subject` text NOT NULL,
  `complaint_body` longtext NOT NULL,
  `complainter_type` varchar(255) NOT NULL,
  `register_date` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint`
--

INSERT INTO `complaint` (`id`, `complainter_id`, `complainter_name`, `branch`, `enrollment_no`, `reciever_id`, `complaint_subject`, `complaint_body`, `complainter_type`, `register_date`, `status`) VALUES
(1, 3, 'Sandip Tawhare', '', '', 2, 'fffdsfs', 'dffafafdfdf', 'Student', '02-Apr-2019 05:37:10 PM', 1),
(2, 3, 'Sandip Tawhare', '', '', 2, 'dgsgsdg', 'gfgsg', 'Student', '02-Apr-2019 06:07:01 PM', 0),
(3, 3, 'Sandip Tawhare', '', '', 2, 'Sandip', 'Tawhare', 'Student', '02-Apr-2019 06:39:09 PM', 0),
(4, 1, 'Sandip Tawhare', 'Computer Engg', '1709920179', 2, 'jjjjj', 'cccccc\r\n', 'Student', '02-Apr-2019 07:16:07 PM', 0),
(5, 1, 'Sandip Tawhare', 'Computer Engg', '1709920179', 2, 'rrtrtertert', 'fgfdgfgd\r\n', 'Student', '02-Apr-2019 07:21:37 PM', 0),
(6, 1, 'Sandip Tawhare', 'Computer Engg', '1709920179', 2, '5448484', '4545454555555', 'Student', '02-Apr-2019 07:23:01 PM', 0),
(7, 1, 'Sandip Tawhare', 'Computer Engg', '1709920179', 2, 'fdfdfdf', 'fvbvbbbbbbb', 'Student', '02-Apr-2019 07:24:51 PM', 0),
(8, 1, 'Sandip Tawhare', 'Computer Engg', '1709920179', 4, 'sdsdsdsdsdsdsdsdsds', 'l;kvkf;lkvvkfelkfxzcvgf\r\ndffcvcvkfdgkcxvkvzfvcxgsgvg\r\ngddgddsgs', 'Student', '02-Apr-2019 10:36:20 PM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_list`
--

CREATE TABLE `user_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `enrollment_no` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `login_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_list`
--

INSERT INTO `user_list` (`id`, `first_name`, `last_name`, `email`, `mobile_no`, `password`, `enrollment_no`, `branch`, `login_type`) VALUES
(1, 'Sandip', 'Tawhare', 'sandiptawhare18081998@rediffmail.com', '9527849688', '12345678', '1709920179', 'Computer Engg', 'Student'),
(2, 'Sandip', 'Tawharevvxvxv', 'sandiptawhare18@gmail.com', '9527849688', '12345678', '', '', 'Administrator'),
(3, 'Sandip', 'Tawhare', 'sandiptawhare18081998@gmail.com', '9527849688', '12345678', '1256789789797', 'Mechanical Engg', 'Parent'),
(4, 'Sanket', 'Doke', 'sandiptawhare18081998@gmail.com', '9527849688', '12345678', '1256789789797', 'Mechanical Engg', 'Teacher'),
(5, 'Sandip', 'Tawharevvxvxv', 'dokesanket27@gmail.com', '9527849688', '', '', 'Computer Engg', 'Teacher'),
(6, 'fgffdgfdgdg', 'dxf', 'mangeshmhaske571@gmail.com', '9527849688', '125125125', '1256459456465464', 'Computer Engg', 'Teacher');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `complaint`
--
ALTER TABLE `complaint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_list`
--
ALTER TABLE `user_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `complaint`
--
ALTER TABLE `complaint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_list`
--
ALTER TABLE `user_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
